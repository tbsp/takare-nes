
PRG_COUNT .equ 1 ;1 = 16KB, 2 = 32KB
MIRRORING .equ %0001 ;%0000 = horizontal, %0001 = vertical, %1000 = four-screen

; iNES header
  .db "NES",$1A  ; magic signature
  .db PRG_COUNT  ; PRG ROM size in 16384 byte units
  .db 1          ; CHR ROM size in 8192 byte units
  .db $00|MIRRORING ;mapper 0 and mirroring
  .db $00        ; PRG RAM size (value of 0 infers 8192 bytes)
  .dsb 8, $00

.base $10000-(PRG_COUNT*$4000)
