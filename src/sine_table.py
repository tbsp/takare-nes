#/bin/python

import math

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

values = []
angle = 0.0
for i in range(256):
    values.append(100 * math.sin(angle))
    angle += (2 * math.pi) / 256.0

for chunk in chunks(values, 16):
    print('[ {} ]'.format(' '.join(['{:02x}'.format(0xff & int(item)) for item in chunk])))
