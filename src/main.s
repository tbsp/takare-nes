;
; Takare NES
; (NROM template Copyright 2011-2014 Damian Yerrick)
; Copyright 2023 Dave VanEe
;
; Copying and distribution of this file, with or without
; modification, are permitted in any medium without royalty provided
; the copyright notice and this notice are preserved in all source
; code copies.  This file is offered as-is, without any warranty.
;

.include "src/nes.inc"
;.include "popslide.inc"

.include "src/vars.s"
.include "src/nrom.s"

__reset:
  .include "src/init.s"

  ; Initialize starting palette
  jsr loadMainPalette

  ; Initialize Uxn
  jsr init@uxn

  ; Initial eval loop
  jsr eval@uxn

  ; Turn the screen on before entering the main loop
  ldx #0
  ldy #0
  lda #VBLANK_NMI|BG_0000|OBJ_1000
  sec
  jsr screenOn@ppu

forever:
  jsr read@pads

  ; TODO: Controller vector handler

  ; Wait for VBlank
  lda nmis
vw3:
  cmp nmis
  beq vw3

  ; Handle Takare frame updates
  jsr frameUpdate@takare

  ; Turn the screen on
  ldx #0
  ldy #0
  lda #VBLANK_NMI|BG_0000|OBJ_1000
  sec
  jsr screenOn@ppu

  jsr screenVector@takare

  jmp forever

loadMainPalette:
  ; seek to the start of palette memory ($3F00-$3F1F)
  ldx #$3F
  stx PPUADDR
  ldx #$00
  stx PPUADDR
@loop:
  lda initialPalette,x
  sta PPUDATA
  inx
  cpx #32
  bcc @loop
  rts

initialPalette:
  .db $0f,$2c,$23,$30,$0f,$2c,$23,$30,$0f,$2c,$23,$30,$0f,$2c,$23,$30
  .db $0f,$2c,$23,$30,$0f,$2c,$23,$30,$0f,$2c,$23,$30,$0f,$2c,$23,$30

__nmi:
  inc nmis
  rti

__irq:
  rti

.include "src/pads.s"
.include "src/ppuclear.s"
.include "src/takare.s"
.include "src/uxn.s"

; Vectors
.org $fffa
  .dw __nmi
  .dw __reset
  .dw __irq

; Include the CHR ROM data
  .incbin "obj/nes/bggfx.chr"
  .incbin "obj/nes/spritegfx.chr"

