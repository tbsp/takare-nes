# Takare (NES)

This is a virtual machine based around the [uxn virtual machine](https://wiki.xxiivv.com/site/uxn.html) CPU, running on the Nintendo Entertainment System. This VM does not attempt to implement the [Varvara devices](https://wiki.xxiivv.com/site/varvara.html), but instead uses a different set of devices which are more easily implemented on the NES hardware. The specific capabilities of the devices implemented are still rooted in Varvara limitations, and therefore only a single 4 color palette is supported at this time.

This isn't really meant as a practical tool, so much as a proof of concept.

# Status

Only the main Uxn CPU core, screen vector, and basic sprite operations are currently supported. Uxn ROMs are manually included at assembly time, as are the CHR tiles. There's essentially zero error/bounds checking in the code, so stack under/overflows, out-of-range memory access, and similar issues will cause all kinds of strange behaviour.

# Building

Originally written for use with ca65, the code has been refactored to build with [asm6](https://www.romhacking.net/utilities/674/) to simplify the build requirements. In addition, the CHR files and takare-demo.rom files are included so building should be as simple as: `asm6 src/main.s takare-nes.nes`.

# Performance

The `takare-demo.tal` animates 64 sprites in a spinning circle, and currently takes 8-9 NES frames per screen vector (~7 FPS). Having dedicated keep/non-keep instruction implementations may reduce redundant stack manipulation, but I expect at most a 10% performance gain from that.

![64 dots in a sqashed circle](images/takare-nes_000.png "takare-demo.tal")

# Limitations

Currently this VM is designed to operate on the bare minimum hardware, and so it targets a simple NROM cartridge. As a result, it's limited to the NES' base 2KB of RAM, and 8KB of CHR ROM.

Since Uxn code tends to depend heavily on self-modifying code, it must run from RAM (not ROM). The NES zero page, stack, shadow OAM, and VRAM transfer buffer (planned) occupy 768 bytes of RAM. An additional 256 bytes are reserved for the Uxn zero page, meaning the actual usable Uxn RAM is only 1024 bytes!

Luckily we don't have to waste any of that on tile data, which is static in CHR ROM. In addition, I intend to allow the LDA instruction to read from ROM directly (for any reads which would land outside RAM), which should allow for lookup tables or other bulk data.

# Specifications

- 256 byte zero page
- 1024 bytes of main Uxn memory
- 32 byte working and return stacks
- 64 bytes of device memory
- Maximum Uxn ROM size of 8KB (perhaps more)

# Devices

Note: The first N (currently 5) ports are the only ones which can trigger DEO callbacks.

| Port  | Function            |
|-------|---------------------|
| 0x00  | Color 0             |
| 0x01  | Color 1             |
| 0x02  | Color 2             |
| 0x03  | Color 3             |
| 0x04  | Sprite operation    |
| 0x05  | Tilemap operation   |
| 0x10  | Screen vector       |
| 0x12  | X                   |
| 0x13  | Y                   |
| 0x14  | Addr                |
| 0x15  | Auto                |
| 0x16  | Repeat              |
| 0x17  | TileID              |
| 0x18  | ScrollX             |
| 0x19  | ScrollY             |

## Colors

For now the color ports simply specify one of the 64 possible [NES palette entries](https://www.nesdev.org/wiki/PPU_palettes#2C02). The 4 colors are applied to both the background and sprites. This may be changed to specifying RGB values which are mapped to the "closest" NES palette entry.

## Sprite Operation

Writing a byte to the sprite port will add a new sprite at the X and Y port coordinates using the tile in the tileID port. The plan is to have the value written to the sprite port specify horizontal/vertical sprite flips, and potentially a palette value, with rotated palette variations automatically available (as a weak proxy to the Varvara blending values).

Every frame if any sprite operations occurred the OAM DMA is triggered to update the hardware sprites. The next frame sprites will once again start populating from the first sprite. This was meant to lessen the load of tracking sprite indicies, though given the current performance the cost of re-adding all sprites every frame (even if only some change) may mean manual index tracking is preferrable.

## Tilemap Operation (Not Implemented)

The tilemap operation will allow setting name table cells to either specific tileID values, or to copy cell values from a given address (Addr). Creative use of the auto and repeat port will allow strips of horizontal/vertical tiles, or even rectangular sections, to be quickly filled/copied.

## Scrolling (Not Implemented)

The scroll registers will likely be fairly seamless pass throughs to hardware scrolling support.
