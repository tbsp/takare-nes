.include "src/nes.inc"

frameUpdate@takare:
  ; Handle pending palette changes
  ldx zPaletteChanged
  beq +
  dex
  stx zPaletteChanged
  ldx #$3F  ; background
  stx PPUADDR
  ldx #$00
  stx PPUADDR
-
  lda zPaletteBuffer,x
  sta PPUDATA
  inx
  cpx #$04
  bne -
  ldx #$3F  ; sprites
  stx PPUADDR
  ldx #$10
  stx PPUADDR
-
  lda zPaletteBuffer-$10,x
  sta PPUDATA
  inx
  cpx #$14
  bne -
+

  ; Update sprites if altered
  ldx zSpritesAltered
  beq +
  dex
  stx zSpritesAltered
  stx zNextOAMIndex
  ; TODO: Fill lingering OAM entries ( not just all trailing ones! )
  ; Perform OAM DMA
  lda #0
  sta OAMADDR
  lda #>OAM
  sta OAM_DMA
+

  rts

screenVector@takare:
_vector = $10
  lda zDevices+_vector
  ora zDevices+_vector+1
  bne @vector_defined
  rts
@vector_defined:
  clc
  lda zDevices+_vector
  adc #>UxnZeroPage
  sta zPC+1
  lda zDevices+_vector+1
  sta zPC
  
  jsr eval@uxn
  rts

sprite@takare:

_sprite .equ $04
_x .equ $12
_y .equ $13
_addr .equ $14
_auto .equ $16
_repeat .equ $17
_tileID .equ $18
_scrolly .equ $19
_scrollx .equ $1a

  ldx zNextOAMIndex
  lda zDevices+_y
  sta OAM,x
  inx
  lda zDevices+_tileID
  sta OAM,x
  inx
  lda #$00
  sta OAM,x
  inx
  lda zDevices+_x
  sta OAM,x
  inx
  stx zNextOAMIndex
  lda #$01
  sta zSpritesAltered

  jmp eval@uxn

palette@takare:
  lda zDevices+0
  sta zPaletteBuffer
  lda zDevices+1
  sta zPaletteBuffer+1
  lda zDevices+2
  sta zPaletteBuffer+2
  lda zDevices+3
  sta zPaletteBuffer+3
  lda #$01
  sta zPaletteChanged
  jmp eval@uxn

; Device Mapping (items with * need DEO handlers)
; - Color 0, 1, 2, 3 [4] ( NES Palette values for now )*
; - screen operation [1]*
; - TBD: audio ports*
; - screen vector [2]
; - x, y [2]
; - addr [2]
; - auto [1]
; - repeat [1]
; - tileID [1]
; - scrolly, scrollx [2]
; - gamepad state [1]

deviceHandlers@takare:
    ; The first 16 devices can have DEO handlers
    .word palette@takare, palette@takare, palette@takare, palette@takare
    .word sprite@takare
    .word $0000, $0000, $0000
    .word $0000, $0000, $0000, $0000, $0000, $0000, $0000, $0000