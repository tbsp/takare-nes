
; Zero Page
.enum $0000

; General
nmis:          .dsb 1
oam_used:      .dsb 1
cur_keys:      .dsb 2
new_keys:      .dsb 2
r0:            .dsb 1
r1:            .dsb 1
r2:            .dsb 1
r3:            .dsb 1
r4:            .dsb 1
r5:            .dsb 1
r6:            .dsb 1
r7:            .dsb 1

; Uxn
zTemp:         .dsb 2
zPC:           .dsb 2

zStacks:
zSP:           .dsb 1  ; stack insertion pointer for instructions
zKeepPtr:      .dsb 1  ; disposable keep pointer

zWSTPtr:       .dsb 1  ; pointer to current working stack entry
zRSTPtr:       .dsb 1  ; pointer to current return stack entry
zWST:          .dsb 32 ; working stack
zRST:          .dsb 32 ; return stack

zSRC:          .dsb 1  ; source stack pointer
zDST:          .dsb 1  ; destination stack pointer

zDevices:      .dsb 64

; Takare
zPaletteChanged:    .dsb 1  ; Flag indicating palette buffer was changed
zPaletteBuffer:     .dsb 4  ; Palette to copy to palette RAM during VBlank if it was changed
zSpritesAltered:    .dsb 1  ; Flag setting if sprites were altered this frame
zNextOAMIndex:      .dsb 1  ; Next OAM index to populate if a sprite is added

.ende

; RAM
.enum $0100
Stack:         .dsb $100
OAM:           .dsb $100
UxnZeroPage:   .dsb $100
UxnProgram:    .dsb $400
.ende