
init@uxn:

  ; Copy the first $400 bytes of Uxn ROM into Uxn RAM
  ldx #$00
@clear_zp:
  lda staticROM@uxn+$000,x
  sta UxnProgram+$000,x
  lda staticROM@uxn+$100,x
  sta UxnProgram+$100,x
  lda staticROM@uxn+$200,x
  sta UxnProgram+$200,x
  lda staticROM@uxn+$300,x
  sta UxnProgram+$300,x
  inx
  bne @clear_zp

  ; Initialize stacks
  lda #zWST
  sta zWSTPtr
  lda #zRST
  sta zRSTPtr

  ; Initialize PC
  lda #<UxnProgram
  sta zPC
  lda #>UxnProgram
  sta zPC+1

  rts

eval@uxn:
  ; Read next instruction, prepare jump offset
  ldx #$00
  lda (zPC,x)
  tay

  ; Increment PC
  clc
  lda zPC
  adc #$01
  sta zPC
  lda zPC+1
  adc #$00
  sta zPC+1

  ; Instruction decoding

  ; Handle special instructions
  ; TODO: Optimize this!
  tya
  cmp #$00
  bne @no_brk
  jmp _BRK
@no_brk:
  cmp #$20
  bne @no_jci
  jmp _JCI
@no_jci:
  cmp #$40
  bne @no_jmi
  jmp _JMI
@no_jmi:
  cmp #$60
  bne @no_jsi
  jmp _JSI
@no_jsi:

  ; bit7 = keep   0x80
  ; bit6 = return 0x40
  ; bit5 = short  0x20

  ldx #zWSTPtr
  tya
  and #$40      ; check for return mode
  beq @no_return
  inx           ; advance to zRSTPtr
@no_return:
  txa
  sta zSRC
  eor #$01
  sta zDST      ; store 'opposite' stack as destination

  tya
  and #$80      ; check for keep mode
  beq @no_keep
  lda $00,x
  sta zKeepPtr
  ldx #zKeepPtr
@no_keep:
  txa
  sta zSP       ; setup (potentially) disposable stack pointer

  tya
  and #$3f      ; calculate jump offset
  asl a
  tax           ; TODO: split high/low table bytes
  lda instrLookup@uxn,x
  sta zTemp
  lda instrLookup@uxn+1,x
  sta zTemp+1

  ; Jump to instruction handler
  ; zKeepPtr = ZP offset to disposable pointer when using keep mode
  ; zSP = ZP offset to "consumable" stack pointer
  ; zSRC = ZP offset to "working" stack pointer (for final results?)
  ; zDST = ZP offset to "return" stack pointer
  jmp (zTemp)

_BRK:
  rts ; return from jsr eval@uxn

_JCI:
  ldx zWSTPtr
  dec zWSTPtr
  lda $ff,x ; flag
  beq @no_jump
  jmp _JMI
@no_jump:
  ; Increment PC past immediate value
  clc
  lda zPC
  adc #$02
  sta zPC
  lda zPC+1
  adc #$00
  sta zPC+1

  jmp eval@uxn

_JMI:
  ; Read immediate value (stored big endian)
  ldy #$00
  lda (zPC),y
  sta r0
  iny
  lda (zPC),y
  sta r1

  ; Increment PC past immediate value
  ; TODO: Combine +2 and offset somehow
  clc
  lda zPC
  adc #$02
  sta zPC
  lda zPC+1
  adc #$00
  sta zPC+1

  ; Add to PC (stored little endian)
  clc
  lda zPC
  adc r1
  sta zPC
  lda zPC+1
  adc r0
  sta zPC+1

  jmp eval@uxn

_JSI:
  ldx zRSTPtr ; push PC+2 to return stack
  inc zRSTPtr
  inc zRSTPtr

  clc
  lda zPC
  adc #$02
  sta $01,x
  lda zPC+1
  adc #$00
  sec
  sbc #>UxnZeroPage
  sta $00,x

  jmp _JMI

_LIT:
  ; Read immediate value
  ldx #$00
  lda (zPC,x)
  tay

  ; Increment PC
  clc
  lda zPC
  adc #$01
  sta zPC
  lda zPC+1
  adc #$00
  sta zPC+1

  ; Push value to stack
  ; TODO: All these $00,x instances feel inefficient, but I'm not sure how to improve on it with two stacks
  ldx zSRC    ; get ZP offset to stack pointer
  lda $00,x   ; read stack pointer value
  inc $00,x   ; increment stack pointer
  tax         ; move stack insertion point to X
  tya         ; recover value
  sta $00,x   ; write value to stack

  jmp eval@uxn

_LIT2:
  ; Read immediate value
  ldy #$00
  lda (zPC),y
  sta r0
  iny
  lda (zPC),y
  tay

  ; Increment PC
  clc
  lda zPC
  adc #$02
  sta zPC
  lda zPC+1
  adc #$00
  sta zPC+1

  ; Push value to stack
  ldx zSRC    ; get ZP offset to stack pointer
  lda $00,x   ; read stack pointer value
  inc $00,x   ; increment stack pointer
  inc $00,x
  tax         ; move stack insertion point to X
  lda r0      ; recover high byte
  sta $00,x   ; write value to stack
  tya         ; recover low byte
  sta $01,x   ; write value to stack

  jmp eval@uxn

_INC:
  ldy zSP
  ldx $00,y
  ldy $ff,x
  iny

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x

  jmp eval@uxn

_INC2:
  ldy zSP
  ldx $00,y
  ldy $ff,x
  iny
  tya
  sta r0
  ldy $fe,x
  cmp #$00  ; TODO: find a way to reuse Z flag from INY
  bne @no_overflow
  iny       ; increment high byte
@no_overflow:
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  sty $00,x
  lda r0
  sta $01,x
  jmp eval@uxn

_POP:
  ldx zSP
  dec $00,x

  jmp eval@uxn

_POP2:
  ldx zSP
  dec $00,x
  dec $00,x

  jmp eval@uxn

_NIP:
  ldy zSP
  ldx $00,y
  ldy $ff,x

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x

  jmp eval@uxn

_NIP2:
  ldy zSP
  ldx $00,y
  lda $ff,x
  sta r0
  ldy $fe,x

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  lda r0
  sta $01,x
  sty $00,x

  jmp eval@uxn

_SWP:
  ldy zSP
  ldx $00,y
  lda $ff,x
  sta r0
  ldy $fe,x

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  lda r0
  sta $00,x
  sty $01,x

  jmp eval@uxn

_SWP2:
  ldy zSP
  ldx $00,y
  lda $fc,x
  sta r0
  lda $fd,x
  sta r1
  lda $fe,x
  sta r2
  ldy $ff,x

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  inc $00,x
  inc $00,x
  tax
  lda r2
  sta $00,x
  sty $01,x
  lda r1
  sta $03,x
  lda r0
  sta $02,x

  jmp eval@uxn

_ROT:
  ldy zSP
  ldx $00,y
  lda $ff,x
  sta r0
  lda $fe,x
  sta r1
  ldy $fd,x

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  inc $00,x
  tax
  lda r1
  sta $00,x
  lda r0
  sta $01,x
  sty $02,x

  jmp eval@uxn

_ROT2:
  ldy zSP
  ldx $00,y
  lda $ff,x
  sta r0
  lda $fe,x
  sta r1
  lda $fd,x
  sta r2
  lda $fc,x
  sta r3
  lda $fb,x
  sta r4
  ldy $fa,x

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  inc $00,x
  inc $00,x
  inc $00,x
  inc $00,x
  tax
  lda r3
  sta $00,x
  lda r2
  sta $01,x
  lda r1
  sta $02,x
  lda r0
  sta $03,x
  sty $04,x
  lda r4
  sta $05,x

  jmp eval@uxn

_DUP:
  ldy zSP
  ldx $00,y
  ldy $ff,x

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  sty $00,x
  sty $01,x

  jmp eval@uxn

_DUP2:
  ldy zSP
  ldx $00,y
  lda $ff,x
  sta r0
  ldy $fe,x

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  inc $00,x
  inc $00,x
  tax
  lda r0
  sty $00,x
  sta $01,x
  sty $02,x
  sta $03,x

  jmp eval@uxn

_OVR:
  ldy zSP
  ldx $00,y
  lda $ff,x
  sta r0
  ldy $fe,x

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  inc $00,x
  tax
  lda r0
  sty $00,x
  sta $01,x
  sty $02,x

  jmp eval@uxn

_OVR2:
  ldy zSP
  ldx $00,y
  lda $ff,x
  sta r0
  lda $fe,x
  sta r1
  lda $fd,x
  sta r2
  ldy $fc,x

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  inc $00,x
  inc $00,x
  inc $00,x
  inc $00,x
  tax
  lda r2
  sty $00,x
  sta $01,x
  sty $04,x
  sta $05,x
  lda r1
  sta $02,x
  lda r0
  sta $03,x

  jmp eval@uxn

_EQU:
  ; A = first value
  ldy zSP
  ldx $00,y
  lda $ff,x

  ; compare to second value
  ldy #$00
  cmp $fe,x
  
  ; write flag to stack
  bne @not_equal
  iny
@not_equal:

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x ; write result to stack

  jmp eval@uxn

_EQU2:
  ; A = first low byte
  ldy zSP
  ldx $00,y
  lda $ff,x

  ; compare to second low byte
  ldy #$00
  cmp $fd,x
  
  bne @not_equal
  ; compare high byte
  lda $fe,x
  cmp $fc,x
  bne @not_equal
  iny
@not_equal:

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x ; write result to stack

  jmp eval@uxn

_NEQ:
  ; A = first value
  ldy zSP
  ldx $00,y
  lda $ff,x

  ; compare to second value
  ldy #$00
  cmp $fe,x
  
  ; write flag to stack
  beq @equal
  iny
@equal:

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x ; write result to stack

  jmp eval@uxn

_NEQ2:
  ; A = first low byte
  ldy zSP
  ldx $00,y
  lda $ff,x

  ; compare to second low byte
  ldy #$00
  cmp $fd,x
  
  bne @not_equal
  ; compare high byte
  lda $fe,x
  cmp $fc,x
  beq @equal
@not_equal:
  iny
@equal:

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x ; write result to stack

  jmp eval@uxn

_GTH:
  ; A = second value
  ldy zSP
  ldx $00,y
  lda $fe,x

  ; compare to first value
  ldy #$00
  cmp $ff,x
  
  ; write flag to stack
  bcc @less_than
  beq @equal
  iny
@less_than:
@equal:

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x ; write result to stack

  jmp eval@uxn

_GTH2:
  ; A = second high byte
  ldy zSP
  ldx $00,y
  lda $fc,x

  ; compare to first high byte
  ldy #$00
  cmp $fe,x
  
  beq @equal_high
  bcs @greater_than
  bcc @less_than
@equal_high:
  ; compare low byte
  lda $fd,x
  cmp $ff,x
  bcc @less_than
  beq @equal_low
@greater_than:
  iny
@less_than:
@equal_low:

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x ; write result to stack

  jmp eval@uxn

_LTH:
  ; A = second value
  ldy zSP
  ldx $00,y
  lda $fe,x

  ; compare to first value
  ldy #$00
  cmp $ff,x
  
  ; write flag to stack
  bcs @geq
  iny
@geq:

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x ; write result to stack

  jmp eval@uxn

_LTH2:
  ; A = second high byte
  ldy zSP
  ldx $00,y
  lda $fc,x

  ; compare to first high byte
  ldy #$00
  cmp $fe,x
  
  beq @equal_high
  bcc @less_than
  bcs @geq
@equal_high:
  ; compare low byte
  lda $fd,x
  cmp $ff,x
  bcs @geq
@less_than:
  iny
@geq:

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x ; write result to stack

  jmp eval@uxn

_JMP:
  ldy zSRC
  ldx $00,y

  clc
  lda $ff,x ; offset
  bpl @not_negative
  dec zPC+1
@not_negative:
  adc zPC
  sta zPC
  lda #$00
  adc zPC+1
  sta zPC+1

  ldx zSP
  dec $00,x

  jmp eval@uxn

_JMP2:
  ldy zSRC
  ldx $00,y

  clc
  lda $ff,x
  sta zPC
  lda $fe,x
  adc #>UxnZeroPage
  sta zPC+1

  ldx zSP
  dec $00,x
  dec $00,x

  jmp eval@uxn

_JCN:
  ldy zSRC
  ldx $00,y
  lda $fe,x ; flag
  beq @no_jump

  clc
  lda $ff,x ; offset
  bpl @not_negative
  dec zPC+1
@not_negative:
  adc zPC
  sta zPC
  lda #$00
  adc zPC+1
  sta zPC+1

@no_jump:
  ldx zSP
  dec $00,x
  dec $00,x

  jmp eval@uxn

_JCN2:
  ldy zSRC
  ldx $00,y
  lda $fd,x ; flag
  beq @no_jump

  lda $ff,x
  sta zPC
  lda $fe,x
  adc #>UxnZeroPage
  sta zPC+1

@no_jump:
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x

  jmp eval@uxn

_JSR:
  ldy zSRC
  ldx $00,y

  ldx zDST ; push PC to other stack
  lda $00,x
  inc $00,x
  inc $00,x
  tax
  lda zPC+1
  sec
  sbc #>UxnZeroPage
  sta $00,x
  lda zPC
  sta $01,x

  clc
  lda $ff,x ; offset
  bpl @not_negative
  dec zPC+1
@not_negative:
  adc zPC
  sta zPC
  lda #$00
  adc zPC+1
  sta zPC+1

  ldx zSP
  dec $00,x

  jmp eval@uxn

_JSR2:
  ldy zSRC
  ldx $00,y

  ldx zDST ; push PC to other stack
  lda $00,x
  inc $00,x
  inc $00,x
  tax
  lda zPC+1
  sec
  sbc #>UxnZeroPage
  sta $00,x
  lda zPC
  sta $01,x

  lda $ff,x
  sta zPC
  lda $fe,x
  adc #>UxnZeroPage
  sta zPC+1

  ldx zSP
  dec $00,x
  dec $00,x

  jmp eval@uxn

_STH:
  ldy zSP
  ldx $00,y
  lda $ff,x
  tay

  ldx zDST
  lda $00,x
  inc $00,x
  tax
  sty $00,x

  ldx zSP
  dec $00,x

  jmp eval@uxn

_STH2:
  ldy zSP
  ldx $00,y
  lda $ff,x
  tay
  lda $fe,x
  sta r0

  ldx zDST
  lda $00,x
  inc $00,x
  inc $00,x
  tax
  lda r0
  sta $00,x
  sty $01,x

  ldx zSP
  dec $00,x
  dec $00,x

  jmp eval@uxn

_LDZ:
  ldy zSP
  ldx $00,y
  lda $ff,x
  tax

  ldy UxnZeroPage,x
  
  ldx zSP
  dec $00,x

  ldx zSRC
  lda $00,x
  inc $00,x
  tax
  sty $00,x

  jmp eval@uxn

_LDZ2:
  ldy zSP
  ldx $00,y
  lda $ff,x
  tax

  ldy UxnZeroPage,x
  lda UxnZeroPage+1,x
  sta r0
  
  ldx zSP
  dec $00,x

  ldx zSRC
  lda $00,x
  inc $00,x
  inc $00,x
  tax
  sty $00,x
  lda r0
  sta $01,x

  jmp eval@uxn

_STZ:
  ldy zSP
  ldx $00,y
  ldy $fe,x
  lda $ff,x
  tax

  tya
  sta UxnZeroPage,x
  
  ldx zSP
  dec $00,x
  dec $00,x

  jmp eval@uxn

_STZ2:
  ldy zSP
  ldx $00,y
  ldy $fd,x
  lda $fe,x
  sta r0
  lda $ff,x
  tax

  tya
  sta UxnZeroPage,x
  lda r0
  sta UxnZeroPage+1,x
  
  ldx zSP
  dec $00,x
  dec $00,x

  jmp eval@uxn

_LDR:
  ldy zSP
  ldx $00,y

  lda zPC   ; copy PC to ZP
  sta r0
  lda zPC+1
  sta r1

  clc
  lda $ff,x ; offset
  bpl @not_negative
  dec r1
@not_negative:
  adc r0
  sta r0
  lda #$00
  tax ; prepare for final lda
  adc r1
  sta r1

  lda (r0,x)
  tay

  ldx zSP
  dec $00,x

  ldx zSRC
  lda $00,x
  inc $00,x
  tax
  sty $00,x

  jmp eval@uxn

_LDR2:
  ldy zSP
  ldx $00,y

  lda zPC   ; copy PC to ZP
  sta r0
  lda zPC+1
  sta r1

  clc
  lda $ff,x ; offset
  bpl @not_negative
  dec r1
@not_negative:
  adc r0
  sta r0
  lda #$00
  tay ; prepare for final lda
  adc r1
  sta r1

  lda (r0),y
  sta r2
  iny
  lda (r0),y
  sta r3

  ldx zSP
  dec $00,x

  ldx zSRC
  lda $00,x
  inc $00,x
  inc $00,x
  tax
  lda r2
  sta $00,x
  lda r3
  sta $01,x

  jmp eval@uxn

_STR:
  ldy zSP
  ldx $00,y
  ldy $fe,x ; value

  lda zPC   ; copy PC to ZP
  sta r0
  lda zPC+1
  sta r1

  clc
  lda $ff,x ; offset
  bpl @not_negative
  dec r1
@not_negative:
  adc r0
  sta r0
  lda #$00
  tax ; prepare for final sta
  adc r1
  sta r1

  tya
  sta (r0,x)

  ldx zSP
  dec $00,x
  dec $00,x

  jmp eval@uxn

_STR2:
  ldy zSP
  ldx $00,y
  lda $fd,x
  sta r2
  lda $fe,x
  sta r3

  lda zPC   ; copy PC to ZP
  sta r0
  lda zPC+1
  sta r1

  clc
  lda $ff,x ; offset
  bpl @not_negative
  dec r1
@not_negative:
  adc r0
  sta r0
  lda #$00
  tay ; prepare for final sta
  adc r1
  sta r1

  lda r2
  sta (r0),y
  iny
  lda r3
  sta (r0),y

  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x

  jmp eval@uxn

_LDA:
  ldy zSP
  ldx $00,y

  ; TODO: Allow loading from beyond Uxn RAM (from Uxn ROM), for out-of-range values

  ; Note: Assumes UxnZeroPage is page-aligned
  lda $ff,x
  sta r0
  lda $fe,x
  clc
  adc #>UxnZeroPage
  sta r1
 
  ldy #$00
  lda (r0),y
  tay

  ldx zSP
  dec $00,x
  dec $00,x

  ldx zSRC
  lda $00,x
  inc $00,x
  tax
  sty $00,x

  jmp eval@uxn

_LDA2:
  ldy zSP
  ldx $00,y

  ; TODO: Allow loading from beyond Uxn RAM (from Uxn ROM), for out-of-range values

  ; Note: Assumes UxnZeroPage is page-aligned
  lda $ff,x
  sta r0
  lda $fe,x
  clc
  adc #>UxnZeroPage
  sta r1
 
  ldy #$00
  lda (r0),y
  sta r2
  iny
  lda (r0),y
  sta r3

  ldx zSP
  dec $00,x
  dec $00,x

  ldx zSRC
  lda $00,x
  inc $00,x
  inc $00,x
  tax
  lda r2
  sta $00,x
  lda r3
  sta $01,x

  jmp eval@uxn

_STA:
  ldy zSP
  ldx $00,y

  ; Note: Assumes UxnZeroPage is page-aligned
  lda $ff,x
  sta r0
  lda $fe,x
  clc
  adc #>UxnZeroPage
  sta r1
 
  lda $fd,x
  ldx #$00
  sta (r0,x)

  ldx zSP
  dec $00,x
  dec $00,x

  jmp eval@uxn

_STA2:
  ldy zSP
  ldx $00,y

  ; Note: Assumes UxnZeroPage is page-aligned
  lda $ff,x
  sta r0
  lda $fe,x
  clc
  adc #>UxnZeroPage
  sta r1
 
  lda $fc,x
  ldy #$00
  sta (r0),y
  iny
  lda $fd,x
  sta (r0),y

  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  jmp eval@uxn

_DEI:
  jmp eval@uxn

_DEI2:
  jmp eval@uxn

_DEO:
  ; A = device port
  ldy zSP
  ldx $00,y
  lda $ff,x

  ; Y = value
  ldy $fe,x

  ; TODO: Fix for keep mode?
  
  ; Write value to device memory
  tax
  sty zDevices,x

  ; sp--
  ldx zSP
  dec $00,x
  dec $00,x

  ; Check if the port has a handler
  cmp #$05
  bcc @has_handler
  jmp eval@uxn
@has_handler:

  ; Jump to handler
  asl a
  tax
  lda deviceHandlers@takare,x
  sta zTemp
  lda deviceHandlers@takare+1,x
  sta zTemp+1

  jmp (zTemp)

_DEO2:
  ldy zSP
  ldx $00,y
  lda $ff,x

  ldy $fe,x
  sty r0
  ldy $fd,x

  ; Write values to device memory
  tax
  sty zDevices,x
  ldy r0
  sty zDevices+1,x

  ; sp--
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x

  ; TODO: Handle handlers on both ports!

  ; Check if the port has a handler
  cmp #$05
  bcc @has_handler
  jmp eval@uxn
@has_handler:

  ; Jump to handler
  asl a
  tax
  lda deviceHandlers@takare,x
  sta zTemp
  lda deviceHandlers@takare+1,x
  sta zTemp+1

  jmp (zTemp)

_ADD:
  ; A = first value
  ldy zSP
  ldx $00,y
  lda $ff,x

  ; Add to second value
  clc
  adc $fe,x
  tay
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x

  jmp eval@uxn

_ADD2:
  ; A = first low byte
  ldy zSP
  ldx $00,y
  lda $ff,x

  ; Add to second low byte
  clc
  adc $fd,x
  sta r0

  ; Add high bytes
  lda $fe,x
  adc $fc,x
  tay
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  sty $00,x
  lda r0
  sta $01,x

  jmp eval@uxn

_SUB:
  ; A = second value
  ldy zSP
  ldx $00,y
  lda $fe,x

  ; Subtract first value
  sec
  sbc $ff,x
  tay
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x

  jmp eval@uxn

_SUB2:
  ; A = second low byte
  ldy zSP
  ldx $00,y
  lda $fd,x

  ; Subtract first low byte
  sec
  sbc $ff,x
  sta r0

  ; Subtract high bytes
  lda $fc,x
  sbc $fe,x
  tay
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  sty $00,x
  lda r0
  sta $01,x

  jmp eval@uxn

_MUL:
  ldy zSP
  ldx $00,y
  lda $ff,x
  sta r0
  lda $fe,x
  sta r1

  ; from The BBC Micro Compendium by Jeremy Ruston: https://archive.org/details/BBCMicroCompendium/page/38/mode/2up
  ; also found in 'Nightshade' for the BBC Micro, http://level7.org.uk/miscellany/nightshade-disassembly.txt (see multiply_A_and_X)

  ; 8 bit x 8 bit unsigned multiply, low 8 bit result
  ; Average cycles: 145
  ; 16 bytes
  ldx #$08
  lda #$00
@loop:
  asl
  asl r0
  bcc @no_add
  clc
  adc r1
@no_add:
  dex
  bne @loop
  tay

  ldx zSP
  dec $00,x
  dec $00,x

  ldx zSRC
  lda $00,x
  inc $00,x
  tax
  sty $00,x

  jmp eval@uxn

_MUL2:
multiplier_low      = r0
multiplier_high     = r1
multiplicand_low    = r2
multiplicand_high   = r3
bit_count           = r4

  ldy zSP
  ldx $00,y
  lda $fc,x
  sta multiplier_high
  lda $fd,x
  sta multiplier_low
  lda $fe,x
  sta multiplicand_high
  lda $ff,x
  sta multiplicand_low

  ; from The Commodore 64 BASIC/KERNAL ROM at $B357: https://github.com/mist64/c64ref/blob/master/Source/c64disasm/c64disasm_en.txt
  ; also in Applesoft II BASIC at $e2b8: https://6502disassembly.com/a2-rom/Applesoft.html

  ; 16 bit x 16 bit unsigned multiply, 16 bit result (overflow detection disabled)
  lda #$10
  sta bit_count
  ldx #$00
  ldy #$00
@loop:
  txa
  asl
  tax
  tya
  rol
  tay
  ;bcs overflow
  asl multiplicand_low
  rol multiplicand_high
  bcc @skip
  clc
  txa
  adc multiplier_low
  tax
  tya
  adc multiplier_high
  tay
  ;bcs overflow
@skip:
  dec bit_count
  bne @loop
;@overflow:
  stx r0
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  sty $00,x
  lda r0
  sta $01,x

  jmp eval@uxn

_DIV:
numerator = r0
denominator = r1
  ldy zSP
  ldx $00,y
  lda $ff,x
  sta denominator
  lda $fe,x
  sta numerator

  lda #$00
  ldx #$07
  clc
@loop:
  rol numerator
  rol
  cmp denominator
  bcc @skip
  sbc denominator
@skip:
  dex
  bpl @loop
  rol numerator

  ldx zSP
  dec $00,x
  dec $00,x

  ldx zSRC
  lda $00,x
  inc $00,x
  tax
  lda numerator
  sta $00,x

  jmp eval@uxn

_DIV2:
dividend    = r0 ; +r1
divisor     = r2 ; +r3
remainder   = r4 ; +r5
result = dividend

  ldy zSP
  ldx $00,y
  lda $fd,x
  sta dividend
  lda $fc,x
  sta dividend+1
  lda $ff,x
  sta divisor
  lda $fe,x
  sta divisor+1

  lda #$00
  sta remainder
  sta remainder+1
  ldx #$10
@loop:
  asl dividend
  rol dividend+1
  rol remainder
  rol remainder+1
  lda remainder
  sec
  sbc divisor
  tay
  lda remainder+1
  sbc divisor+1
  bcc @skip
  sta remainder+1
  sty remainder
  inc result
@skip:
  dex
  bne @loop
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  lda result
  sta $01,x
  lda result+1
  sta $00,x

  jmp eval@uxn

_AND:
  ldy zSP
  ldx $00,y
  lda $ff,x

  and $fe,x
  tay
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x

  jmp eval@uxn

_AND2:
  ldy zSP
  ldx $00,y
  lda $ff,x

  and $fd,x
  sta r0

  lda $fe,x
  and $fc,x
  tay
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  sty $00,x
  lda r0
  sta $01,x

  jmp eval@uxn

_ORA:
  ldy zSP
  ldx $00,y
  lda $ff,x

  ora $fe,x
  tay
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x

  jmp eval@uxn

_ORA2:
  ldy zSP
  ldx $00,y
  lda $ff,x

  ora $fd,x
  sta r0

  lda $fe,x
  ora $fc,x
  tay
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  sty $00,x
  lda r0
  sta $01,x

  jmp eval@uxn

_EOR:
  ldy zSP
  ldx $00,y
  lda $ff,x

  eor $fe,x
  tay
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  sty $00,x

  jmp eval@uxn

_EOR2:
  ldy zSP
  ldx $00,y
  lda $ff,x

  eor $fd,x
  sta r0

  lda $fe,x
  eor $fc,x
  tay
  
  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  sty $00,x
  lda r0
  sta $01,x

  jmp eval@uxn

_SFT:
  ldy zSP
  ldx $00,y
  lda $fe,x
  sta r0

  lda $ff,x
  and #$0f
  beq @done_right_shift
  tay
@right_shift:
  lsr r0
  dey
  bne @right_shift
@done_right_shift:
  lda $ff,x
  and #$f0
  beq @done_left_shift
  lsr a
  lsr a
  lsr a
  lsr a
  tay
@left_shift:
  asl r0
  dey
  bne @left_shift
@done_left_shift:

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  tax
  lda r0
  sta $00,x

  jmp eval@uxn

_SFT2:
  ldy zSP
  ldx $00,y
  lda $fd,x
  sta r0
  lda $fe,x
  sta r1

  lda $ff,x
  and #$0f
  beq @done_right_shift
  tay
@right_shift:
  lsr r0
  ror r1
  dey
  bne @right_shift
@done_right_shift:
  lda $ff,x
  and #$f0
  beq @done_left_shift
  lsr a
  lsr a
  lsr a
  lsr a
  tay
@left_shift:
  asl r1
  rol r0
  dey
  bne @left_shift
@done_left_shift:

  ; decrement SP before writing result (if using keep this doesn't affect the real stack pointer)
  ldx zSP
  dec $00,x
  dec $00,x
  dec $00,x

  ; Write result to stack
  ldx zSRC  ; read pointer to where result should go
  lda $00,x ; read ZP offset for stack
  inc $00,x ; increment pointer for item we're about to push
  inc $00,x
  tax
  lda r0
  sta $00,x
  lda r1
  sta $01,x

  jmp eval@uxn

instrLookup@uxn:
    ;    0x00   0x01   0x02   0x03   0x04   0x05   0x06   0x07   0x08   0x09   0x0A   0x0B   0x0C   0x0D   0x0E   0x0F
    .dw  _LIT,  _INC,  _POP,  _NIP,  _SWP,  _ROT,  _DUP,  _OVR,  _EQU,  _NEQ,  _GTH,  _LTH,  _JMP,  _JCN,  _JSR,  _STH
    .dw  _LDZ,  _STZ,  _LDR,  _STR,  _LDA,  _STA,  _DEI,  _DEO,  _ADD,  _SUB,  _MUL,  _DIV,  _AND,  _ORA,  _EOR,  _SFT
    .dw _LIT2, _INC2, _POP2, _NIP2, _SWP2, _ROT2, _DUP2, _OVR2, _EQU2, _NEQ2, _GTH2, _LTH2, _JMP2, _JCN2, _JSR2, _STH2
    .dw _LDZ2, _STZ2, _LDR2, _STR2, _LDA2, _STA2, _DEI2, _DEO2, _ADD2, _SUB2, _MUL2, _DIV2, _AND2, _ORA2, _EOR2, _SFT2

staticROM@uxn:
  .incbin "obj/nes/takare-demo.rom"

